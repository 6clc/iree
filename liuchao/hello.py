import tensorflow.compat.v2 as tf
model_path = '/home/liuchao/prs/models/tf'


def parse_signature():
  loaded_model = tf.saved_model.load(model_path)
  call = loaded_model.__call__.get_concrete_function(
           tf.TensorSpec([1, 224, 224, 3], tf.float32))
  signatures = {'predict': call}
  tf.saved_model.save(loaded_model, model_path, signatures=signatures)
  print(list(loaded_model.signatures.keys()))

# parse_signature()
loaded_model = tf.saved_model.loaded(model_path)
print(list(loaded_model.signatures.keys()))
