pushd ~/prs/iree
cmake -GNinja -B ../iree-build/ -S . \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo \
        -DIREE_ENABLE_ASSERTIONS=ON \
            -DCMAKE_C_COMPILER=clang \
                -DCMAKE_CXX_COMPILER=clang++ \
                    -DIREE_ENABLE_LLD=ON \
-DIREE_HAL_DRIVER_DYLIB=ON \
-DIREE_TARGET_BACKEND_DYLIB_LLVM_AOT=ON
popd
