# Copyright 2021 The IREE Authors
#
# Licensed under the Apache License v2.0 with LLVM Exceptions.
# See https://llvm.org/LICENSE.txt for license information.
# SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception

steps:
  - label: ":zap: Build with tracing enabled"
    commands:
      - "git submodule sync && git submodule update --init --jobs 8 --depth 1"
      - "docker run --user=$(id -u):$(id -g) --volume=\\$PWD:\\$IREE_DOCKER_WORKDIR --workdir=\\$IREE_DOCKER_WORKDIR --rm gcr.io/iree-oss/base@sha256:ea076afa9ec854ed75d0608a08d64e4bc5e1ad2933d497912b4034553d4a56ac ./build_tools/cmake/build_tracing.sh"
    env:
      IREE_DOCKER_WORKDIR: "/usr/src/github/iree"
    agents:
      - "queue=build"

  - label: ":hammer_and_wrench: Build the runtime only"
    commands:
      - "git submodule sync && git submodule update --init --jobs 8 --depth 1"
      - "docker run --user=$(id -u):$(id -g) --volume=\\$PWD:\\$IREE_DOCKER_WORKDIR --workdir=\\$IREE_DOCKER_WORKDIR --rm gcr.io/iree-oss/base@sha256:ea076afa9ec854ed75d0608a08d64e4bc5e1ad2933d497912b4034553d4a56ac ./build_tools/cmake/build_runtime.sh"
    env:
      IREE_DOCKER_WORKDIR: "/usr/src/github/iree"
    agents:
      - "queue=build"

  - label: ":pinching_hand: Build the size-optimized runtime only"
    commands:
      - "git submodule sync && git submodule update --init --jobs 8 --depth 1"
      - "docker run --user=$(id -u):$(id -g) --volume=\\$PWD:\\$IREE_DOCKER_WORKDIR --workdir=\\$IREE_DOCKER_WORKDIR --rm gcr.io/iree-oss/base@sha256:ea076afa9ec854ed75d0608a08d64e4bc5e1ad2933d497912b4034553d4a56ac ./build_tools/cmake/build_runtime_small.sh"
    env:
      IREE_DOCKER_WORKDIR: "/usr/src/github/iree"
    agents:
      - "queue=build"

  - label: ":gnu: Build with GCC"
    key: "build-gcc"
    commands:
      - "git submodule sync && git submodule update --init --jobs 8 --depth 1"
      - "docker run --env CC=/usr/bin/gcc-9 --env CXX=/usr/bin/g++-9 --user=$(id -u):$(id -g) --volume=\\$PWD:\\$IREE_DOCKER_WORKDIR --workdir=\\$IREE_DOCKER_WORKDIR --rm gcr.io/iree-oss/base@sha256:ea076afa9ec854ed75d0608a08d64e4bc5e1ad2933d497912b4034553d4a56ac ./build_tools/cmake/clean_build.sh"
    env:
      IREE_DOCKER_WORKDIR: "/usr/src/github/iree"
    agents:
      - "queue=build"

  - label: ":linux: Build host install"
    key: "build-host-install"
    commands:
      - "git submodule sync && git submodule update --init --jobs 8 --depth 1"
      - "docker run --user=$(id -u):$(id -g) --volume=\\$PWD:\\$IREE_DOCKER_WORKDIR --workdir=\\$IREE_DOCKER_WORKDIR --rm gcr.io/iree-oss/base@sha256:ea076afa9ec854ed75d0608a08d64e4bc5e1ad2933d497912b4034553d4a56ac ./build_tools/cmake/build_host_install.sh"
      - "tar -czvf build-artifacts.tgz build-host/install"
    artifact_paths: "build-artifacts.tgz"
    env:
      IREE_DOCKER_WORKDIR: "/usr/src/github/iree"
    agents:
      - "queue=build"

  - label: ":webassembly: Build WebAssembly runtime with Emscripten"
    depends_on: "build-host-install"
    commands:
      - "buildkite-agent artifact download --step build-host-install build-artifacts.tgz ./"
      - "tar xzf build-artifacts.tgz"
      - "git submodule update --init --jobs 8 --depth 1"
      - "docker run --user=$(id -u):$(id -g) --volume=\\$PWD:\\$IREE_DOCKER_WORKDIR --workdir=\\$IREE_DOCKER_WORKDIR --rm gcr.io/iree-oss/emscripten@sha256:2d99a86203c4de572a8928892cbfcd41d8e16ca432c53ad01ed33d2fb991ad78 ./build_tools/cmake/build_runtime_emscripten.sh"
    env:
      IREE_DOCKER_WORKDIR: "/usr/src/github/iree"
    agents:
      - "queue=build"
